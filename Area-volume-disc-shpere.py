######################################################################
#Area and Volume of Disc and Spheres
#Calculates the area of disc (circles) and the volume of spheres
#This program uses the diameter
#
#Distance in cm
#
#By: Carolton Tippitt
######################################################################
#
#Version 1.01 17-04-14 Added text align right
#Version	1.00 17-04-09
#Version 0.05 17-04-04 WX deprecated class PySimpleApp changed to App(False)
#Version 0.01 16-11-01
#
######################################################################
import wx
from math import pi
from sys import exit

def GetArea(Diameter):
	#Uses the diameter instead of the radius
	return float(pi*(Diameter/2)**2)

def GetVolume(Diameter):
  #uses the diameter instead of the radius
  return float(pi*4./3*(Diameter/2)**2)

class MyFrame(wx.Frame):
	def __init__(self,parent,id,title):
		wx.Frame.__init__(self,parent,id,title,wx.DefaultPosition,wx.Size(240,270))
		
		self.panel=wx.Panel(self)
		#making a check box to choose between volume and area
		wx.StaticBox(self.panel,-1,'Calculate area or volume? (Pick One)',pos=(5,10),size=(230,50))
		self.Box1=wx.CheckBox(self.panel,-1,'Area',pos=(20,30))
		self.Box2=wx.CheckBox(self.panel,-1,'Volume',pos=(100,30))
		
		wx.StaticText(self.panel,-1,'Diameter:',pos=(7,70))
		self.Input1=wx.TextCtrl(self.panel,-1,pos=(120,65),size=(75,25),style=wx.TE_PROCESS_ENTER|wx.TE_RIGHT)
		wx.StaticText(self.panel,-1,'cm',pos=(200,70))
		
		self.Butt1=wx.Button(self.panel,-1,'Calculate',pos=(60,100))
		
		wx.StaticText(self.panel,-1,'Volume',pos=(5,140))
		self.OutPut1=wx.TextCtrl(self.panel,-1,pos=(120,135),size=(75,25),style=wx.TE_READONLY|wx.TE_RIGHT)
		wx.StaticText(self.panel,-1,'cm^3',pos=(200,140))
		
		wx.StaticText(self.panel,-1,'Area',pos=(5,170))
		self.OutPut2=wx.TextCtrl(self.panel,-1,pos=(120,165),size=(75,25),style=wx.TE_READONLY|wx.TE_RIGHT)
		wx.StaticText(self.panel,-1,'cm^2',pos=(200,170))
		
		self.Butt2=wx.Button(self.panel,-1,'Quit',(60,200))
		
		#makes a menu bar containing only file->quit
		menu_bar=wx.MenuBar()
		file_menu=wx.Menu()
		file_menu.Append(wx.ID_EXIT,'&Quit\tCTRL+Q')
		menu_bar.Append(file_menu,'&File')
		self.SetMenuBar(menu_bar)
		
		#making all the buttons work
		self.Input1.Bind(wx.EVT_TEXT_ENTER,self.WeGotThis)
		self.Bind(wx.EVT_BUTTON, self.WeGotThis,self.Butt1)
		self.Bind(wx.EVT_BUTTON,self.getQuit,self.Butt2)
		self.Bind(wx.EVT_MENU,self.getQuit,id=wx.ID_EXIT)
		
	def WeGotThis(self,event):
		#keeps the GUI updating until quite
		D=float(self.Input1.GetValue())
		switch1=self.Box1.GetValue()
		switch2=self.Box2.GetValue()
		if switch1==switch2:
			self.OutPut1.ChangeValue('%s'%('Error'))
			self.OutPut2.ChangeValue('%s'%('Error'))
			self.Box1.SetValue(False)
			self.Box2.SetValue(False)
		
		elif switch1==1:
			A=GetArea(D)
			self.OutPut1.ChangeValue('********')
			self.OutPut2.ChangeValue('%.2f'%A)
			self.Box1.SetValue(False)
			self.Box2.SetValue(False)
			
		elif switch2==1:
			V=GetVolume(D)
			self.OutPut1.ChangeValue('%.2f'%V)
			self.OutPut2.ChangeValue('********')
			self.Box1.SetValue(False)
			self.Box2.SetValue(False)
	
	def getQuit(self,event):
		self.Destroy()
	
class MyApp(wx.App):
	def OnInit(self):
		frame=MyFrame(None,-1,'Area Volume')
		frame.Show(True)
		self.SetTopWindow(frame)
		return True

app=MyApp()
app.MainLoop()
