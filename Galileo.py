######################################################################
#Galileo's programs
#Calculates the time squared and Acceleration of mass on incline plain
#assumes starting from rest
#
#Time in Sec
#Distance in cm
#
#By: Carolton Tippitt
######################################################################
#
#Version 1.00 17-04-14 added Right alignment
#Version 0.05 17-04-04 WX deprecated class PySimpleApp changed to App(False)
#Version 0.01 16-11-01
#
######################################################################
import wx
from sys import exit

def GetTT(time):
	return time**2

def GetAcceleration(time2,dis):
	return 2*dis/time2

class MyFrame(wx.Frame):
	def __init__(self,parent,id,title):
		wx.Frame.__init__(self,parent,id,title,wx.DefaultPosition,wx.Size(270,260))
		
		self.panel=wx.Panel(self)
		
		wx.StaticText(self.panel,-1,'Avg. Time:',pos=(5,10))
		self.Input1=wx.TextCtrl(self.panel,-1,pos=(120,5),size=(75,25),style=wx.TE_PROCESS_ENTER|wx.TE_RIGHT)
		wx.StaticText(self.panel,-1,'Sec',pos=(200,10))
		
		wx.StaticText(self.panel,-1,'Distance:',pos=(5,50))
		self.Input2=wx.TextCtrl(self.panel,-1,pos=(120,40),size=(75,25),style=wx.TE_PROCESS_ENTER|wx.TE_RIGHT)
		wx.StaticText(self.panel,-1,'cm',pos=(200,50))
		
		self.Butt1=wx.Button(self.panel,-1,label='Calculate',pos=(80,80))
		
		wx.StaticText(self.panel,-1,'Avg. Time Squared:',pos=(5,125))
		self.Output1=wx.TextCtrl(self.panel,-1,pos=(120,120),size=(75,25),style=wx.TE_READONLY|wx.TE_RIGHT)
		wx.StaticText(self.panel,-1,'sec^2',pos=(200,125))
		
		wx.StaticText(self.panel,-1,'Acceleration:',(5,160))
		self.Output2=wx.TextCtrl(self.panel,-1,pos=(120,155),size=(75,25),style=wx.TE_READONLY|wx.TE_RIGHT)
		wx.StaticText(self.panel,-1,'cm/s^2',(200,160))
		
		self.Butt2=wx.Button(self.panel,-1,label='Quit',pos=(80,190))
		
		#makes a menu bar containing only file->quit
		menu_bar=wx.MenuBar()
		file_menu=wx.Menu()
		file_menu.Append(wx.ID_EXIT,'&Quit\tCTRL+Q')
		menu_bar.Append(file_menu,'&File')
		self.SetMenuBar(menu_bar)
		
		self.Input1.Bind(wx.EVT_TEXT_ENTER,self.WeGotThis)
		self.Input2.Bind(wx.EVT_TEXT_ENTER,self.WeGotThis)
		self.Bind(wx.EVT_BUTTON,self.WeGotThis,self.Butt1)
		self.Bind(wx.EVT_BUTTON,self.getQuit,self.Butt2)
		self.Bind(wx.EVT_MENU,self.getQuit,id=wx.ID_EXIT)
		
	def WeGotThis(self,event):
		self.Output1.ChangeValue('%.2f'%(GetTT(float(self.Input1.GetValue()))))
		self.Output2.ChangeValue('%.2f'%(GetAcceleration(GetTT(float(self.Input1.GetValue())),float(self.Input2.GetValue()))))
	
	def getQuit(self,event):
		self.Destroy()

class MyApp(wx.App):
	def OnInit(self):
		frame=MyFrame(None,-1,'Galileo')
		frame.Show(True)
		self.SetTopWindow(frame)
		return True

app=MyApp()
app.MainLoop()
