######################################################################
#Wave Visualization
#This program allows students to visualize waves interacting with each other.
#
#By: Carolton Tipitt
######################################################################
#
#Version 0.01 01-02-2018
#

#import matplotlib.pyplot as plt

import matplotlib
matplotlib.use('WXAgg')

from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.backends.backend_wx import NavigationToolbar2Wx
from matplotlib.figure import Figure

from matplotlib.pyplot import close,figure,plot,show,subplot,xlabel,ylabel,title

from numpy import sin,pi,cos,arange,linspace,radians

import wx
from wx import App,Frame,StaticText,Choice,TextCtrl,Button,EVT_BUTTON,EVT_MENU
from wx import TE_PROCESS_ENTER,TE_PROCESS_TAB,TE_RIGHT,TE_READONLY,EVT_TEXT_ENTER
from wx import TE_MULTILINE,Slider
from wx.lib.plot import PlotCanvas, PlotGraphics, PolyLine, PolyMarker

from sys import exit

close('all')


def getWaveEq(x,phi,A,l):
	"""
	l is in nm
	v=3*10^8m/s
	"""
	k=2*pi/(l*10**(-9))
	return A*sin(k*x+phi)

x=linspace(0,4*pi,200)

angle1=0
angle2=0
phi1=radians(angle1)
phi2=radians(angle2)
lambda1=500
lambda2=500
A1=1
A2=1

class MyFrame(wx.Frame):
	def __init__(self,parent,id,title):
		wx.Frame.__init__(self,parent,id,title,wx.DefaultPosition,wx.Size(350,200))
		
		self.panel=wx.Panel(self)
		
		self.bigbox=wx.BoxSizer(wx.VERTICAL)
		self.littlebox=wx.BoxSizer(wx.VERTICAL)
		self.box1=wx.BoxSizer(wx.HORIZONTAL)
		self.box2=wx.BoxSizer(wx.HORIZONTAL)
		self.box3=wx.BoxSizer(wx.HORIZONTAL)
		self.box4=wx.BoxSizer(wx.HORIZONTAL)
		
		self.figure=Figure()
		self.axes1=self.figure.add_subplot(311)
		self.axes2=self.figure.add_subplot(312)
		self.axes3=self.figure.add_subplot(313)
		self.axes1.set_ylim([-20,20])
		self.axes2.set_ylim([-20,20])
		self.axes3.set_ylim([-20,20])
		#self.canvas=FigureCanvas(self, -1, self.figure)

		
		self.wave=StaticText(self.panel,-1,'Waves',pos=(5,10))
		self.wave1=StaticText(self.panel,-1,'Wave 1',pos=(135,10))
		self.wave2=StaticText(self.panel,-1,'Wave 2',pos=(250,10))
		self.length1=StaticText(self.panel,-1,'Wave Length',pos=(5,35))
		self.amp1=StaticText(self.panel,-1,'Amplitude',pos=(5,60))
		self.phase1=StaticText(self.panel,-1,'Phase',pos=(5,85))
	
		self.Wave1Length=Slider(self.panel,-1,400,400,600,size=(100,10),pos=(100,35))
		self.Wave2Length=Slider(self.panel,-1,400,400,600,size=(100,10),pos=(210,35))
		
		self.Wave1Amp=Slider(self.panel,-1,0,0,20,size=(100,10),pos=(100,60))
		self.Wave2Amp=Slider(self.panel,-1,0,0,20,size=(100,10),pos=(210,60))
		
		self.Wave1Phase=Slider(self.panel,-1,0,0,180,size=(100,10),pos=(100,85))
		self.Wave2Phase=Slider(self.panel,-1,0,0,180,size=(100,10),pos=(210,85))
		
		self.PButt=wx.Button(self.panel,-1,label='Plot',pos=(80,120))
		self.QButt=wx.Button(self.panel,-1,label='Quit',pos=(180,120))
		
		self.box1.Add(self.wave,0,wx.LEFT|wx.TOP|wx.GROW)
		self.box1.Add(self.wave1,0,wx.LEFT|wx.TOP|wx.GROW)
		self.box1.Add(self.wave2,0,wx.LEFT|wx.TOP|wx.GROW)
		
		self.box2.Add(self.length1,0,wx.LEFT|wx.TOP|wx.GROW)
		self.box2.Add(self.Wave1Length,0,wx.LEFT|wx.TOP|wx.GROW)
		self.box2.Add(self.Wave2Length,0,wx.LEFT|wx.TOP|wx.GROW)
		
		self.box3.Add(self.amp1,0,wx.LEFT|wx.TOP|wx.GROW)
		self.box3.Add(self.Wave1Amp,0,wx.LEFT|wx.TOP|wx.GROW)
		self.box3.Add(self.Wave2Amp,0,wx.LEFT|wx.TOP|wx.GROW)
		
		self.box4.Add(self.phase1,0,wx.LEFT|wx.TOP|wx.GROW)
		self.box4.Add(self.Wave1Phase,0,wx.LEFT|wx.TOP|wx.GROW)
		self.box4.Add(self.Wave2Phase,0,wx.LEFT|wx.TOP|wx.GROW)
		
		self.littlebox.Add(self.box1,0,wx.LEFT|wx.TOP|wx.GROW)
		self.littlebox.Add(self.box2,0,wx.LEFT|wx.TOP|wx.GROW)
		self.littlebox.Add(self.box3,0,wx.LEFT|wx.TOP|wx.GROW)
		self.littlebox.Add(self.box4,0,wx.LEFT|wx.TOP|wx.GROW)
		
		self.bigbox.Add(self.littlebox,0,wx.LEFT|wx.TOP|wx.GROW)
		self.bigbox.AddSpacer(10)
		self.bigbox.Add(self.axes1,1,wx.RIGHT|wx.TOP|wx.GROW)
		
		self.Fit()
		
		#frame2=GFrame(self)
		#frame2.Show(True)
		
		#makes a menu bar containing only file->quit
		menu_bar=wx.MenuBar()
		file_menu=wx.Menu()
		file_menu.Append(wx.ID_EXIT,'&Quit\tCTRL+Q')
		menu_bar.Append(file_menu,'&File')
		self.SetMenuBar(menu_bar)
		
		self.Bind(wx.EVT_BUTTON,self.WeGotThis,self.PButt)
		self.Bind(wx.EVT_BUTTON,self.getQuit,self.QButt)
		self.Bind(wx.EVT_MENU,self.getQuit,id=wx.ID_EXIT)
		#self.Bind(wx.EVT_MENU,self.WeGotThis,id=wx.ID_ANY)
	
	def WeGotThis(self,event):
		L1=float(self.Wave1Length.GetValue())
		A1=float(self.Wave1Amp.GetValue())
		P1=float(self.Wave1Phase.GetValue())
		L2=float(self.Wave2Length.GetValue())
		A2=float(self.Wave2Amp.GetValue())
		P2=float(self.Wave2Phase.GetValue())
		
		wave1=getWaveEq(x,P1,A1,L1)
		wave2=getWaveEq(x,P2,A2,L2)
		wave3=wave1+wave2
		
		self.axes1.clear()
		self.axes2.clear()
		self.axes3.clear()
		self.axes1.plot(x,wave1)
		self.axes2.plot(x,wave2)
		self.axes3.plot(x,wave3)
		self.canvas.draw()
		
		#frame2=GFrame(self)
		#frame2.getPlot(phi1,A1,lambda1,phi2,A2,lambda2)
		
		#figure(1)
		#subplot(311)
		#plot(x,wave1)
		#ylabel('A')
		#title('Wave 1')
		#subplot(312)
		#plot(x,wave2)
		#ylabel('A')
		#title('Wave 2')
		#subplot(313)
		#plot(x,wave3)
		#ylabel('A')
		#title('Wave 1 + Wave 2')
		#show()

	def getQuit(self,event):
		self.Destroy()

#class GFrame(wx.Frame):
	#def __init__(self,parent):
		#wx.Frame.__init__(self,parent)
		
		#self.Gpanel=wx.Panel(self)
		#self.figure=Figure()
		#self.axes1=self.figure.add_subplot(311)
		#self.axes2=self.figure.add_subplot(312)
		#self.axes3=self.figure.add_subplot(313)
		#self.axes1.set_ylim([-20,20])
		#self.axes2.set_ylim([-20,20])
		#self.axes3.set_ylim([-20,20])
		#self.canvas=FigureCanvas(self, -1, self.figure)
		#self.Fit()
		
	#def getPlot(self,phi1,A1,lambda1,phi2,A2,lambda2):
		#print "getPlot in Gframe worked"
		#wave1=getWaveEq(x,phi1,A1,lambda1)
		#wave2=getWaveEq(x,phi2,A2,lambda2)
		#wave3=wave1+wave2
		#self.axes1.clear()
		#self.axes2.clear()
		#self.axes3.clear()
		#self.axes1.plot(x,wave1)
		#self.axes2.plot(x,wave2)
		#self.axes3.plot(x,wave3)
		#self.canvas.draw()
	
	#def close(self,evt):
		#self.MakeModal(False)
		#evt.Skip()

class MyApp(wx.App):
	def OnInit(self):
		frame=MyFrame(None,-1,'Wave Visualization')
		frame.Show(True)
		self.SetTopWindow(frame)
		return True

app=MyApp()
app.MainLoop()

#wave1=getWaveEq(x,phi1,A1,lambda1)
#wave2=getWaveEq(x,phi2,A2,lambda2)

#if (abs(angle1-angle2)==90) and (lambda1==lambda2):
	#wave3=getSimWave(x,0,0)
#else:
#wave3=wave1+wave2

#figure(1)
#subplot(311)
#plot(x,wave1)
#ylabel('A')
#subplot(312)
#plot(x,wave2)
#ylabel('A')
#subplot(313)
#plot(x,wave3)
#xlabel('Wave 1 + Wave 2')
#ylabel('A')
#show()
