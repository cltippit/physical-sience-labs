import numpy as np
from random import randint
import wx


class MyFrame1 ( wx.Frame ):
	def __init__( self, parent, id, title ):
		wx.Frame.__init__ ( self, parent, id, title,wx.DefaultPosition, size = wx.Size(500,300))
		self.panel =wx.Panel(self)
		
		wx.StaticText(self.panel, -1, 'GRE Vocabulary game!', pos=(150,5))
		self.button1 = wx.Button(self.panel, -1, 'Start', pos=(150,25))
		wx.StaticText(self.panel,-1,'Name a word that means...', pos=(120,45))
		self.meaning =wx.TextCtrl(self.panel,-1,pos=(120,65), size = (200,40))
		self.button2 = wx.Button(self.panel, -1,'Enter!', pos=(150,85))
		self.word = wx.TextCtrl(self.panel, -1, pos=(120,105), size = (200, 40))
		wx.StaticText(self.panel,-1,'Your answer is:', pos=(125, 120))
		self.ans = wx.TextCtrl(self.panel, -1, pos=(120,145), size=(200, 40))
		self.button3 = wx.Button(self.panel, -1,'Quit', pos=(120,165))
        
class myApp(wx.App):
	def OnInit(self):
		frame1= MyFrame1(None, -1, 'Final Project')
		frame1.Show(True)
		
		self.SetTopWindow(frame1)
		return True

app = myApp()
app.MainLoop()
