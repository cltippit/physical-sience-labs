######################################################################
#Snell's Law
#
#Calculate the speed of light in a material.
#For use in Physical Science Laws and Physics Labs.
#Reflection-Refraction
#
#By: Carolton Tipitt
######################################################################
#
#Version 1.00	17-04-14
#Version 0.01	17-04-11
#
######################################################################
import wx
from sys import exit
from numpy import deg2rad,sin

class MyFrame(wx.Frame):
	def __init__(self,parent,id,title):
		wx.Frame.__init__(self,parent,id,title,wx.DefaultPosition,wx.Size(233,250))

		wx.StaticText(self,-1,'Incident:',(5,10))
		#the style here allows enter to be pressed
		self.Input1=wx.TextCtrl(self,-1,pos=(120,5),size=(75,25),style=wx.TE_PROCESS_ENTER|wx.TE_RIGHT)
		wx.StaticText(self,-1,'deg',(200,10))

		wx.StaticText(self,-1,'Refracted:',(5,50))
		self.Input2=wx.TextCtrl(self,-1,pos=(120,40),size=(75,25),style=wx.TE_PROCESS_ENTER|wx.TE_RIGHT)
		wx.StaticText(self,-1,'deg',(200,50))

		self.butt1=wx.Button(self,-1,'Calculate',(60,80))

		wx.StaticText(self,-1,'Velocity:',(5,125))
		#the style here makes it read only
		self.Output1=wx.TextCtrl(self,-1,pos=(110,120),size=(85,25),style=wx.TE_READONLY|wx.TE_RIGHT)
		wx.StaticText(self,-1,'m/s',(200,125))
		
		self.butt2=wx.Button(self,-1,'Quit',(60,170))
		
		#makes a menu bar containing only file->quit
		menu_bar=wx.MenuBar()
		file_menu=wx.Menu()
		file_menu.Append(wx.ID_EXIT,'&Quit\tCTRL+Q')
		menu_bar.Append(file_menu,'&File')
		self.SetMenuBar(menu_bar)
		
		#making all the buttons work
		self.Input1.Bind(wx.EVT_TEXT_ENTER,self.WeGotThis)
		self.Input2.Bind(wx.EVT_TEXT_ENTER,self.WeGotThis)
		self.Bind(wx.EVT_BUTTON, self.WeGotThis,self.butt1)
		self.Bind(wx.EVT_BUTTON,self.getQuit,self.butt2)
		self.Bind(wx.EVT_MENU,self.getQuit,id=wx.ID_EXIT)
	
	def WeGotThis(self,event):
		deg1=float(self.Input1.GetValue())
		deg2=float(self.Input2.GetValue())
		angle=3e+8*sin(deg2rad(deg2))/sin(deg2rad(deg1))
		self.Output1.ChangeValue('%.4e'%(angle))
		
	def getQuit(self,event):
		self.Destroy()
	
class MyApp(wx.App):
	def OnInit(self):
		frame=MyFrame(None,-1,'Snell\'s Law')
		frame.Show(True)
		self.SetTopWindow(frame)
		return True
  
app=MyApp()
app.MainLoop()
