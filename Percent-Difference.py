######################################################################
#Percent Error
#Calculate the percent error of measurements
#For use in Physical Science Laws and Physics Labs.
#
#By: Carolton Tipitt
######################################################################
#Version 1.00 17-04-07
#Version 0.05 17-04-04 WX deprecated class PySimpleApp changed to App(False)
#Version 0.01 16-11-02
#
######################################################################
import wx
from sys import exit

def GetPError(value1,value2):
	a=(value1-value2)/((value1+value2)*0.5)*100
	return abs(a)

class MyFrame(wx.Frame):
	def __init__(self,parent,id,title):
		wx.Frame.__init__(self,parent,id,title,wx.DefaultPosition,wx.Size(233,220))
		
		self.panel=wx.Panel(self)
		
		wx.StaticText(self.panel,-1,'First Value:',(5,10))
		#the style here allows enter to be pressed
		self.Input1=wx.TextCtrl(self.panel,-1,pos=(120,5),size=(75,25),style=wx.TE_PROCESS_ENTER|wx.TE_RIGHT)
	
		wx.StaticText(self.panel,-1,'Second Value:',(5,50))
		self.Input2=wx.TextCtrl(self.panel,-1,pos=(120,40),size=(75,25),style=wx.TE_PROCESS_ENTER|wx.TE_RIGHT)
	
		self.butt1=wx.Button(self.panel,-1,'Calculate',(60,80))
	
		wx.StaticText(self.panel,-1,'Percent Difference:',(5,125))
		#the style here makes it read only
		self.Output1=wx.TextCtrl(self.panel,-1,pos=(120,120),size=(75,25),style=wx.TE_READONLY|wx.TE_RIGHT)
		wx.StaticText(self.panel,-1,'%',(200,125))
		
		self.butt2=wx.Button(self.panel,-1,'Quit',(60,155))
		
		#makes a menu bar containing only file->quit
		menu_bar=wx.MenuBar()
		file_menu=wx.Menu()
		file_menu.Append(wx.ID_EXIT,'&Quit\tCTRL+Q')
		menu_bar.Append(file_menu,'&File')
		self.SetMenuBar(menu_bar)
		
		#making all the buttons work
		self.Input1.Bind(wx.EVT_TEXT_ENTER,self.WeGotThis)
		self.Input2.Bind(wx.EVT_TEXT_ENTER,self.WeGotThis)
		self.Bind(wx.EVT_BUTTON, self.WeGotThis,self.butt1)
		self.Bind(wx.EVT_BUTTON,self.getQuit,self.butt2)
		self.Bind(wx.EVT_MENU,self.getQuit,id=wx.ID_EXIT)
	
	def WeGotThis(self,event):
		Act=float(self.Input1.GetValue())
		Theo=float(self.Input2.GetValue())
		PE=GetPError(Act,Theo)
		self.Output1.ChangeValue('%.2f'%(PE))
	
	def getQuit(self,event):
		self.Destroy()
	
class MyApp(wx.App):
	def OnInit(self):
		frame=MyFrame(None,-1,'Percent Difference')
		frame.Show(True)
		self.SetTopWindow(frame)
		return True
  
app=MyApp()
app.MainLoop()
