######################################################################
#Energy
#Calculate the Potential Energy, Velocity Squared, and Kinetic Energy
#inputs Mass of car, Height of car from ground, velocity of car
#
#Gravity is set to 10m/s^2
#
#By: Carolton Tipitt
######################################################################
#
#Version 2.01	17-04-14
#Version 2.00	17-04-07
#Version 1.01  17-04-05
#Version 1.00	16-11-02
#
######################################################################

import wx
from sys import exit

def GetKE(mass,velocity):
	return float(.5*mass*velocity**2)

def GetPE(mass,hieght,acc):
	return float(mass*hieght*acc)

class MyFrame(wx.Frame):
	def __init__(self,parent,id,title):
		wx.Frame.__init__(self,parent,id,title,wx.DefaultPosition,wx.Size(260,325))
		
		self.panel=wx.Panel(self)
		
		wx.StaticText(self.panel,-1,'Mass:',pos=(5,10))
		self.Input1=wx.TextCtrl(self.panel,-1,pos=(120,5),size=(75,25),style=wx.TE_PROCESS_ENTER|wx.TE_RIGHT)
		wx.StaticText(self.panel,-1,'kg',(200,10))
		
		wx.StaticText(self.panel,-1,'Velocity:',pos=(5,50))
		self.Input2=wx.TextCtrl(self.panel,pos=(120,45),size=(75,25),style=wx.TE_PROCESS_ENTER|wx.TE_RIGHT)
		wx.StaticText(self.panel,-1,'m/s',(200,50))
		
		wx.StaticText(self.panel,-1,'Height:',pos=(5,90))
		self.Input3=wx.TextCtrl(self.panel,pos=(120,85),size=(75,25),style=wx.TE_PROCESS_ENTER|wx.TE_RIGHT)
		wx.StaticText(self.panel,-1,'m',pos=(200,90))
		
		self.Butt1=wx.Button(self.panel,-1,label='Calculate',pos=(60,120))
		
		wx.StaticText(self.panel,-1,'Velocity Squared:',pos=(5,160))
		self.Output1=wx.TextCtrl(self.panel,-1,pos=(120,155),size=(75,25),style=wx.TE_READONLY|wx.TE_RIGHT)
		wx.StaticText(self.panel,-1,'m^2/s^2',pos=(200,160))
		
		wx.StaticText(self.panel,-1,'Kinetic Energy:',pos=(5,200))
		self.Output2=wx.TextCtrl(self.panel,-1,pos=(120,195),size=(75,25),style=wx.TE_READONLY|wx.TE_RIGHT)
		wx.StaticText(self.panel,-1,'J',pos=(200,200))
		
		wx.StaticText(self.panel,-1,'Potential Energy:',pos=(5,240))
		self.Output3=wx.TextCtrl(self.panel,-1,pos=(120,235),size=(75,25),style=wx.TE_READONLY|wx.TE_READONLY|wx.TE_RIGHT)
		wx.StaticText(self.panel,-1,'J',pos=(200,240))
		
		self.Butt2=wx.Button(self.panel,-1,label='Quit',pos=(60,270))
		
		#makes a menu bar containing only file->quit
		menu_bar=wx.MenuBar()
		file_menu=wx.Menu()
		file_menu.Append(wx.ID_EXIT,'&Quit\tCTRL+Q')
		menu_bar.Append(file_menu,'&File')
		self.SetMenuBar(menu_bar)
		
		self.Input1.Bind(wx.EVT_TEXT_ENTER,self.WeGotThis)
		self.Input2.Bind(wx.EVT_TEXT_ENTER,self.WeGotThis)
		self.Input3.Bind(wx.EVT_TEXT_ENTER,self.WeGotThis)
		self.Bind(wx.EVT_BUTTON,self.WeGotThis,self.Butt1)
		self.Bind(wx.EVT_BUTTON,self.getQuit,self.Butt2)
		self.Bind(wx.EVT_MENU,self.getQuit,id=wx.ID_EXIT)
	
	def WeGotThis(self,event):
		mass=float(self.Input1.GetValue())
		velocity=float(self.Input2.GetValue())
		hieght=float(self.Input3.GetValue())
		acc=10
		velocitysquared=velocity**2
		KE=GetKE(mass,velocity)
		PE=GetPE(mass,hieght,acc)
		self.Output1.ChangeValue('%.2f'%(velocitysquared))
		self.Output2.ChangeValue('%.2f'%(KE))
		self.Output3.ChangeValue('%.2f'%(PE))

	def getQuit(self,event):
		self.Destroy()

class MyApp(wx.App):
	def OnInit(self):
		frame=MyFrame(None,-1,'Energy')
		frame.Show(True)
		self.SetTopWindow(frame)
		return True

app=MyApp()
app.MainLoop()
