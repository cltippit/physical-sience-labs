######################################################################
#Wave Visualization
#This program allows students to visualize waves interacting with each other.
#
#By: Carolton Tipitt
######################################################################
#
#Version 1.01 01-02-2018
#

#import matplotlib.pyplot as plt

import matplotlib
matplotlib.use('WXAgg')

from matplotlib import animation

from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.backends.backend_wx import NavigationToolbar2Wx as NavigationToolbar
from matplotlib.figure import Figure

from matplotlib.pyplot import close,figure,plot,show,subplot,xlabel,ylabel,title

from numpy import pi,sin,cos,arange,linspace,radians

import wx
from wx import App,Frame,StaticText,Choice,TextCtrl,Button,EVT_BUTTON,EVT_MENU
from wx import TE_PROCESS_ENTER,TE_PROCESS_TAB,TE_RIGHT,TE_READONLY,EVT_TEXT_ENTER
from wx import TE_MULTILINE,Slider
from wx.lib.plot import PlotCanvas, PlotGraphics, PolyLine, PolyMarker

from sys import exit

close('all')


def getWaveEq(x,phi,A,l):
	"""
	l is in nm
	v=3*10^8m/s
	"""
	#k=2*pi/(l*10**(-9))
	k=l
	return A*sin(k*x-phi)

x=linspace(0,6*pi,400)

class Gpanel(wx.Panel):
	def __init__(self,parent):
		wx.Panel.__init__(self,parent)
		
		self.figure=Figure()
		self.axes1=self.figure.add_subplot(311)
		self.axes2=self.figure.add_subplot(312)
		self.axes3=self.figure.add_subplot(313)
		self.axes1.set_title('Wave 1')
		self.axes2.set_title('Wave 2')
		self.axes3.set_title('Wave 3')
		self.axes1.set_ylim([-40,40])
		self.axes1.set_xlim([0,14])
		self.axes2.set_ylim([-40,40])
		self.axes2.set_xlim([0,14])
		self.axes3.set_ylim([-40,40])
		self.axes3.set_xlim([0,14])
		self.axes1.get_xaxis().set_visible(False)
		self.axes2.get_xaxis().set_visible(False)
		self.axes3.get_xaxis().set_visible(False)
		self.canvas=FigureCanvas(self, -1, self.figure)
		self.getStartWave()
		
	def getStartWave(self):
		wave1=getWaveEq(x,0,1,1)
		wave2=getWaveEq(x,0,1,1)
		wave3=wave1+wave2
		self.getPlot(wave1,wave2,wave3)
		
	def getPlot(self,wave1,wave2,wave3):
		self.axes1.clear()
		self.axes2.clear()
		self.axes3.clear()
		self.axes1.set_title('Wave 1')
		self.axes2.set_title('Wave 2')
		self.axes3.set_title('Wave 3')
		self.axes1.set_ylim([-40,40])
		self.axes1.set_xlim([0,6*3.14])
		self.axes2.set_ylim([-40,40])
		self.axes2.set_xlim([0,6*3.14])
		self.axes3.set_ylim([-40,40])
		self.axes3.set_xlim([0,14])
		self.axes1.get_xaxis().set_visible(False)
		self.axes2.get_xaxis().set_visible(False)
		self.axes3.get_xaxis().set_visible(False)
		self.axes1.plot(x,wave1)
		self.axes2.plot(x,wave2)
		self.axes3.plot(x,wave3)
		self.canvas.draw()
		
	#def animate(self,i):
		#self.axes1.clear()
		#self.axes2.clear()
		#self.axes3.clear()
		#self.axes1.set_title('Wave 1')
		#self.axes2.set_title('Wave 2')
		#self.axes3.set_title('Wave 3')
		#self.axes1.set_ylim([-40,40])
		#self.axes1.set_xlim([0,6*3.14])
		#self.axes2.set_ylim([-40,40])
		#self.axes2.set_xlim([0,6*3.14])
		#self.axes3.set_ylim([-40,40])
		#self.axes3.set_xlim([0,14])
		

class MainFrame(wx.Frame):
	def __init__(self,parent,id,title):
		wx.Frame.__init__(self,parent,id,title,wx.DefaultPosition,wx.Size(650,700),style=wx.MINIMIZE_BOX|wx.SYSTEM_MENU|wx.CAPTION|wx.CLOSE_BOX|wx.CLIP_CHILDREN)
		
		self.spwindow=wx.SplitterWindow(self)
		self.Gpanel=Gpanel(self.spwindow)
		self.MainPanel=wx.Panel(self.spwindow,style=wx.SUNKEN_BORDER)
		
		self.spwindow.SplitHorizontally(self.Gpanel,self.MainPanel,470)
		
		self.wave=StaticText(self.MainPanel,-1,'Waves',pos=(5,10))
		self.wave1=StaticText(self.MainPanel,-1,'Wave 1',pos=(135,10))
		self.wave2=StaticText(self.MainPanel,-1,'Wave 2',pos=(250,10))
		self.length1=StaticText(self.MainPanel,-1,'Wave Length',pos=(5,35))
		self.amp1=StaticText(self.MainPanel,-1,'Amplitude',pos=(5,60))
		self.phase1=StaticText(self.MainPanel,-1,'Phase',pos=(5,85))
		
		self.Wave1Length=Slider(self.MainPanel,-1,1,1,15,size=(100,10),pos=(100,35))
		#self.Wave1Length.SetLineSize(1)
		self.Wave2Length=Slider(self.MainPanel,-1,1,1,15,size=(100,10),pos=(210,35))
		#self.Wave2Length.SetLineSize(1)
		
		self.Wave1Amp=Slider(self.MainPanel,-1,1,0,20,size=(100,10),pos=(100,60))
		#self.Wave1Amp.SetLineSize(1)
		self.Wave2Amp=Slider(self.MainPanel,-1,1,0,20,size=(100,10),pos=(210,60))
		#self.Wave2Amp.SetLineSize(1)
		
		self.Wave1Phase=Slider(self.MainPanel,-1,0,0,2*3.14,size=(100,10),pos=(100,85))
		self.Wave1Phase.SetTickFreq(3.14/16)#addes tick marks to the slider.  only works for MSW
		#self.Wave1Phase.SetLineSize(40)
		self.Wave2Phase=Slider(self.MainPanel,-1,0,0,2*3.14,size=(100,10),pos=(210,85))
		self.Wave2Phase.SetTickFreq(3.14/16)#addes tick marks to the slider.  only works for MSW
		#self.Wave2Phase.SetLineSize(40)
		
		self.RButt=wx.Button(self.MainPanel,-1,label='Reset',pos=(80,120))
		self.OffsetButt=wx.Button(self.MainPanel,-1,label='Out of Phase', pos=(180,120))
		self.QButt=wx.Button(self.MainPanel,-1,label='Quit',pos=(120,150))
		
		#makes a menu bar containing only file->quit
		menu_bar=wx.MenuBar()
		file_menu=wx.Menu()
		file_menu.Append(wx.ID_EXIT,'&Quit\tCTRL+Q')
		menu_bar.Append(file_menu,'&File')
		self.SetMenuBar(menu_bar)
		
		self.Bind(wx.EVT_BUTTON,self.getReset,self.RButt)
		self.Bind(wx.EVT_BUTTON,self.getOutofphase,self.OffsetButt)
		self.Bind(wx.EVT_BUTTON,self.getQuit,self.QButt)
		self.Bind(wx.EVT_MENU,self.getQuit,id=wx.ID_EXIT)
		self.Bind(wx.EVT_SCROLL,self.WeGotThis,self.Wave1Length)
		self.Bind(wx.EVT_SCROLL,self.WeGotThis,self.Wave2Length)
		self.Bind(wx.EVT_SCROLL,self.WeGotThis,self.Wave1Amp)
		self.Bind(wx.EVT_SCROLL,self.WeGotThis,self.Wave2Amp)
		self.Bind(wx.EVT_SCROLL,self.WeGotThis,self.Wave1Phase)
		self.Bind(wx.EVT_SCROLL,self.WeGotThis,self.Wave2Phase)
	
	def WeGotThis(self,event):
		L1=float(self.Wave1Length.GetValue())
		A1=float(self.Wave1Amp.GetValue())
		P1=float(self.Wave1Phase.GetValue())
		L2=float(self.Wave2Length.GetValue())
		A2=float(self.Wave2Amp.GetValue())
		P2=float(self.Wave2Phase.GetValue())
		
		wave1=getWaveEq(x,P1,A1,L1)
		wave2=getWaveEq(x,P2,A2,L2)
		wave3=wave1+wave2
		
		self.Gpanel.getPlot(wave1,wave2,wave3)
	
	def getOutofphase(self,event):
		maxLen=self.Wave1Length.GetMax()
		maxAmp=self.Wave1Amp.GetMax()
		minPhase=self.Wave1Phase.GetMin()
		maxPhase=self.Wave1Phase.GetMax()
		self.Wave1Length.SetValue(maxLen/2.0)
		self.Wave2Length.SetValue(maxLen/2.0)
		self.Wave1Amp.SetValue(maxAmp/2.0)
		self.Wave2Amp.SetValue(maxAmp/2.0)
		self.Wave1Phase.SetValue(0)
		self.Wave2Phase.SetValue(3.14)
		
		wave1=getWaveEq(x,minPhase,maxAmp,maxLen/2.0)
		wave2=getWaveEq(x,3.14,maxAmp,maxLen/2.0)
		wave3=wave1+wave2
		
		self.Gpanel.getPlot(wave1,wave2,wave3)
	
	def getReset(self,event):
		minLen=self.Wave1Length.GetMin()
		minAmp=self.Wave1Amp.GetMin()
		minPhase=self.Wave1Phase.GetMin()
		self.Wave1Length.SetValue(minLen)
		self.Wave2Length.SetValue(minLen)
		self.Wave1Amp.SetValue(minAmp+1)
		self.Wave2Amp.SetValue(minAmp+1)
		self.Wave1Phase.SetValue(minPhase)
		self.Wave2Phase.SetValue(minPhase)
		self.Gpanel.getStartWave()
	
	def getQuit(self,event):
		self.Destroy()

class MyApp(wx.App):
	def OnInit(self):
		frame=MainFrame(None,-1,'Wave Visualization')
		frame.Show(True)
		self.SetTopWindow(frame)
		return True

app=MyApp()
app.MainLoop()
