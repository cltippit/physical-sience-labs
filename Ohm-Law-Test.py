######################################################################
#Ohm's Law
#Calculate the resistance and power of a series and parallel circuit.
#For use in Physical Science Laws and Physics Labs.
#By: Carolton Tipitt
######################################################################
#
#Version 2.01	17-04-14
#Version 2.00	17-04-14
#Version 1.01  17-04-05
#Version 1.00	16-11-02
#
######################################################################
import wx
from sys import exit

def GetResistance(v,i):
	#Using Ohm's Law to find the Resistance
	return float(v/i)

def GetPower(v,i):
	#Using the voltage and current the user provides so there is no
	#additional rounding errors
	return float(i*v)

class MyPanel(wx.Panel):
	def __init__(self,parent):
		wx.Panel.__init__(self,parent)
		
		wx.StaticText(self,-1,'Voltage:',(5,10))
		#the style here allows enter to be pressed
		self.Input1=wx.TextCtrl(self,-1,pos=(120,5),size=(75,25),style=wx.TE_PROCESS_ENTER|wx.TE_RIGHT)
		wx.StaticText(self,-1,'V',(200,10))
			
		wx.StaticText(self,-1,'Current:',(5,50))
		self.Input2=wx.TextCtrl(self,-1,pos=(120,40),size=(75,25),style=wx.TE_PROCESS_ENTER|wx.TE_RIGHT)
		wx.StaticText(self,-1,'A',(200,50))
		
		self.butt1=wx.Button(self,-1,'Calculate',(60,80))
		
		wx.StaticText(self,-1,'Resistance:',(5,125))
		#the style here makes it read only
		self.Output1=wx.TextCtrl(self,-1,pos=(120,120),size=(75,25),style=wx.TE_READONLY|wx.TE_RIGHT)
		wx.StaticText(self,-1,'Ohm',(200,125))
		
		wx.StaticText(self,-1,'Power Dissipated:',(5,160))
		self.Output2=wx.TextCtrl(self,-1,pos=(120,155),size=(75,25),style=wx.TE_READONLY|wx.TE_RIGHT)
		wx.StaticText(self,-1,'Watt',(200,160))
		
		self.butt2=wx.Button(self,-1,'Quit',(60,190))
		
		#making all the buttons work
		self.Input1.Bind(wx.EVT_TEXT_ENTER,self.WeGotThis)
		self.Input2.Bind(wx.EVT_TEXT_ENTER,self.WeGotThis)
		self.Bind(wx.EVT_BUTTON, self.WeGotThis,self.butt1)
		self.Bind(wx.EVT_BUTTON,self.getQuit,self.butt2)
	
	def WeGotThis(self,event):
		V=float(self.Input1.GetValue())
		I=float(self.Input2.GetValue())
		R=GetResistance(V,I)
		W=GetPower(V,I)
		self.Output1.ChangeValue('%.4f'%(R))
		self.Output2.ChangeValue('%.4f'%(W))
	
	def getQuit(self,event):
		exit()

class MyFrame(wx.Frame):
	def __init__(self,parent,id,title):
		wx.Frame.__init__(self,parent,id,title,wx.DefaultPosition,wx.Size(233,250))
		
		self.mypanel=MyPanel(self)
		self.sizer=wx.BoxSizer(wx.VERTICAL)
		self.sizer.Add(self.mypanel,1,wx.EXPAND)
		self.SetSizer(self.sizer)
		
		#makes a menu bar containing only file->quit
		menu_bar=wx.MenuBar()
		file_menu=wx.Menu()
		file_menu.Append(wx.ID_EXIT,'&Quit\tCTRL+Q')
		menu_bar.Append(file_menu,'&File')
		self.SetMenuBar(menu_bar)
		
		self.Bind(wx.EVT_MENU,self.getQuit,id=wx.ID_EXIT)
	
	
	def getQuit(self,event):
		self.Destroy()
	
class MyApp(wx.App):
	def OnInit(self):
		frame=MyFrame(None,-1,'Ohm''s Law')
		frame.Show(True)
		self.SetTopWindow(frame)
		return True
  
app=MyApp()
app.MainLoop()
