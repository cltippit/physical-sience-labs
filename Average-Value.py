######################################################################
#Average Value
#
#Calculate the average of a group of numbers.
#For use in Physical Science Laws and Physics Labs.
#
#By: Carolton Tipitt
######################################################################
#
#Version 1.00	17-04-14
#Version 0.01	17-04-11
#
######################################################################
import wx
from sys import exit
#from numpy import sum, array

valueList=[]

class MyFrame(wx.Frame):
	def __init__(self,parent,id,title):
		wx.Frame.__init__(self,parent,id,title,wx.DefaultPosition,wx.Size(250,270))
		
		self.panel=wx.Panel(self)
		
		wx.StaticText(self.panel,-1,'Value:',pos=(5,10))
		self.Input1=wx.TextCtrl(self.panel,-1,pos=(120,5),size=(75,25),style=wx.TE_PROCESS_ENTER|wx.TE_RIGHT)
		
		self.Butt1=wx.Button(self.panel,-1,'Add Value',pos=(25,40))
		self.Butt2=wx.Button(self.panel,-1,'Remove Value',pos=(120,40))
		
		wx.StaticText(self.panel,-1,'Current Values:',pos=(5,70))
		self.Output1=wx.TextCtrl(self.panel,-1,pos=(5,85),size=(240,40),style=wx.TE_MULTILINE|wx.TE_READONLY|wx.TE_WORDWRAP)
		
		self.Butt3=wx.Button(self.panel,-1,'Average',pos=(70,130))
		
		wx.StaticText(self.panel,-1,'Average Value',pos=(5,165))
		self.Output2=wx.TextCtrl(self.panel,-1,pos=(120,160),size=(75,25),style=wx.TE_READONLY|wx.TE_RIGHT)
		
		self.Butt4=wx.Button(self.panel,-1,'Quit',pos=(25,200))
		self.Butt5=wx.Button(self.panel,-1,'Clear All',pos=(120,200))
		
		#makes a menu bar containing only file->quit
		menu_bar=wx.MenuBar()
		file_menu=wx.Menu()
		file_menu.Append(wx.ID_EXIT,'&Quit\tCTRL+Q')
		menu_bar.Append(file_menu,'&File')
		self.SetMenuBar(menu_bar)
		
		#making all the buttons work

		self.Input1.Bind(wx.EVT_TEXT_ENTER,self.getvalues)
		self.Bind(wx.EVT_BUTTON, self.getvalues,self.Butt1)
		self.Bind(wx.EVT_BUTTON,self.getRemove,self.Butt2)
		self.Bind(wx.EVT_BUTTON,self.getAvg,self.Butt3)
		self.Bind(wx.EVT_BUTTON,self.getQuit,self.Butt4)
		self.Bind(wx.EVT_BUTTON,self.getReset,self.Butt5)
		self.Bind(wx.EVT_MENU,self.getQuit,id=wx.ID_EXIT)
		
	
	def getRemove(self,event):
		rValue=float(self.Input1.GetValue())
		i=len(valueList)-1
		while i>=0:
			if rValue==valueList[i]:
				del valueList[i]
				self.Output1.Clear()
				for i in valueList:
					self.Output1.AppendText('%g, '%(float(i)))
				break
			i-=1
	
	def getReset(self,event):
		self.Output1.Clear()
		self.Input1.Clear()
		self.Output2.Clear()
		valueList=[]
	
	def getvalues(self,event):
		valueList.append(float(self.Input1.GetValue()))
		self.Output1.AppendText('%g, '%(float(self.Input1.GetValue())))
		self.Input1.Clear()
	
	def getAvg(self,event):
		ans=sum(valueList)/float(len(valueList))
		self.Output2.ChangeValue('%.2f'%ans)
	
	def getQuit(self,event):
		self.Destroy()
	
class MyApp(wx.App):
	def OnInit(self):
		frame=MyFrame(None,-1,'Average Value')
		frame.Show(True)
		self.SetTopWindow(frame)
		return True
  
app=MyApp()
app.MainLoop()
