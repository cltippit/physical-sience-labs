######################################################################
#Percent Error
#Calculate the percent error of measurements
#For use in Physical Science Laws and Physics Labs.
#
#Time in Sec
#Distance in cm
#
#By: Carolton Tippitt
######################################################################
#
#Version	1.01 17-04-14 Added alignment to the right
#Version	1.00 17-04-09
#Version 0.05 17-04-04 WX deprecated class PySimpleApp changed to App(False)
#Version 0.01 16-11-01
#
######################################################################

import wx
from sys import exit

def GetPError(Act,Theo):
	'''
	Your basic percent difference formula
	'''
	a=(Theo-Act)/Theo*100
	#returns only positive values
	return abs(a)

def GetTT(time):
	'''
	squaring the time for returning to the user
	'''
	return time**2

def GetAcceleration(time2,dis):
	'''
	returns the acceleration using basic Newton Physics
	'''
	return 2*dis/time2

#this portion makes the GUI
#Please refer to wxPython for more specifics in this portion
class MyFrame(wx.Frame):
	def __init__(self,parent,id,title):
		#the size portion makes the 'frame' or window a specific size
		wx.Frame.__init__(self,parent,id,title,wx.DefaultPosition,wx.Size(233,220))
		
		self.panel=wx.Panel(self)
		
		#static text means unchangeable
		wx.StaticText(self.panel,-1,'Measured:',pos=(5,10))
		#textctrl is a readable, type-able and write-able box
		#the last part makes it able to accept enter
		self.Input1=wx.TextCtrl(self.panel,-1,pos=(120,5),size=(75,25),style=wx.TE_PROCESS_ENTER|wx.TE_RIGHT)
		
		wx.StaticText(self.panel,-1,'True:',pos=(5,45))
		self.Input2=wx.TextCtrl(self.panel,-1,pos=(120,40),size=(75,25),style=wx.TE_PROCESS_ENTER|wx.TE_RIGHT)
		
		#makes a button
		self.Butt1=wx.Button(self.panel,-1,label='Calculate',pos=(60,80))
		
		wx.StaticText(self.panel,-1,'Percent Error:',pos=(5,125))
		self.Output1=wx.TextCtrl(self.panel,-1,pos=(120,120),size=(75,25),style=wx.TE_READONLY|wx.TE_RIGHT)
		wx.StaticText(self.panel,-1,'%',pos=(200,125))
		
		self.Butt2=wx.Button(self.panel,-1,label='Quit',pos=(60,155))
		
		#makes a menu bar containing only file->quit
		menu_bar=wx.MenuBar()
		file_menu=wx.Menu()
		file_menu.Append(wx.ID_EXIT,'&Quit\tCTRL+Q')
		menu_bar.Append(file_menu,'&File')
		self.SetMenuBar(menu_bar)
		
		#bins buttons and pressing enter to certain functions
		self.Input1.Bind(wx.EVT_TEXT_ENTER,self.WeGotThis)
		self.Input2.Bind(wx.EVT_TEXT_ENTER,self.WeGotThis)
		self.Bind(wx.EVT_BUTTON,self.WeGotThis,self.Butt1)
		self.Bind(wx.EVT_BUTTON,self.getQuit,self.Butt2)
		self.Bind(wx.EVT_MENU,self.getQuit,id=wx.ID_EXIT)
	
	def WeGotThis(self,event):
		'''
		main function that retrieves the users input values
		and then acts on them
		'''
		Act=float(self.Input1.GetValue())
		Theo=float(self.Input2.GetValue())
		PE=GetPError(Act,Theo)
		self.Output1.ChangeValue('%.2f'%(PE))
	
	def getQuit(self,event):
		'''
		exits the app when exec
		'''
		self.Destroy()

class MyApp(wx.App):
	'''
	makes the frame or window the app runs in and files
	it with what is specified above
	'''
	def OnInit(self):
		frame=MyFrame(None,-1,'Percent Error')
		frame.Show(True)
		self.SetTopWindow(frame)
		return True

#makes the app and runs it in a loop till quit
app=MyApp()
app.MainLoop()
